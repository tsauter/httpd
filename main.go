package main

import (
	"flag"
	"fmt"
	"github.com/urfave/negroni"
	"log"
	"net"
	"net/http"
	"os"
)

var (
	listen    = flag.String("listen", ":8080", "listen on for incoming requests")
	rootDir   = flag.String("rootdir", "", "root directory for serving files")
	allowedIP = flag.String("allowedIP", "", "which ip address is allowed to connect")
	tls       = flag.Bool("tls", true, "server files via TLS")
	version   = flag.Bool("version", false, "show version")

	logger *log.Logger

	Version   = "xx"
	BuildTime = "xx"
	BuildUser = "xx"
	BuildHost = "xx"
)

func main() {
	flag.Parse()

	logger = log.New(os.Stdout, "[httpd] ", 0) //log.LstdFlags)

	if *version {
		fmt.Printf("httpd: v%s (builded at %s on %s@%s)\n", Version,
			BuildTime, BuildUser, BuildHost)
		os.Exit(1)
	}

	if *rootDir == "" {
		pwd, err := os.Getwd()
		if err != nil {
			logger.Fatalf("Failed to get current working directory: %s", err.Error())
		}
		*rootDir = pwd
	}

	logger.Printf("Serving files from %s", *rootDir)

	if *allowedIP != "" {
		logger.Printf("Restricting server for %s\n", *allowedIP)
	} else {
		logger.Printf("WARNING: server is open for all clients!!!")
	}

	fs := http.FileServer(http.Dir(*rootDir))
	secure := SecureHandler{FileServer: fs, AllowedIP: *allowedIP}

	mux := http.NewServeMux()
	mux.Handle("/", secure)

	logger.Printf("Listening on %s...\n", *listen)
	n := negroni.New()

	recovery := negroni.NewRecovery()
	n.Use(recovery)

	rlogger := negroni.NewLogger()
	rlogger.SetFormat("{{.StartTime}} | {{.Status}} | \t {{.Duration}} | {{.Hostname}} | {{.Request.RemoteAddr}} | {{.Method}} {{.Path}}")
	n.Use(rlogger)

	n.UseHandler(mux)

	if *tls {
		tls_cert_filename := "public.pem"
		tls_key_filename := "private.pem"
		logger.Fatal(http.ListenAndServeTLS(*listen, tls_cert_filename, tls_key_filename, n))
	} else {
		logger.Printf("WARNING: no transport layer encryption")
		logger.Fatal(http.ListenAndServe(*listen, n))
	}
}

type SecureHandler struct {
	FileServer http.Handler
	AllowedIP  string
}

func (sh SecureHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if (sh.AllowedIP != "") && (ip != sh.AllowedIP) {
		logger.Printf("IP rejected: %s\n", ip)
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	sh.FileServer.ServeHTTP(w, r)
}
