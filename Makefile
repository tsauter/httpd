
NAME=httpd
VERSION=0.3
DATE=$(shell C:/MinGW/msys/1.0/bin/date.exe +%FT%T%z)
BUILDUSER=$(USERNAME)
HOSTNAME=$(shell C:/MinGW/msys/1.0/bin/hostname.exe)
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.BuildTime=${DATE} -X main.BuildHost=${HOSTNAME} -X main.BuildUser=${BUILDUSER}"
MODULES=main.go

run:
	go run ${LDFLAGS} ${MODULES}

install: vet test
	go install ${LDFLAGS} -v

build: vet test
	go build ${LDFLAGS} -v -o ${NAME}.exe ${MODULES}

fmt:
	go fmt

test:
	go test

# https://github.com/golang/lint
lint:
	golint

# https//godoc.org/golang.org/x/tools/cmd/vet
vet:
	go vet

godep:
	godep save

clean:
	del ${NAME}.exe

distclean: clean

