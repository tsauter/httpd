# httpd

Httpd is a tiny web server to serve static files out of a directory.

## Run

The following command serves the files from c:/mydata and configures the listen
port to 3000 on all interfaces.

```
httpd --listen ":3000" --rootdir c:/mydata
```

